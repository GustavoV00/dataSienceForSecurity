# TAREFA
1. Faça uma captura de rede em sua máquina usando o ScaPy (aproximadamente 1 minuto, navegando na Web) e salve o arquivo como "trace.pcap"
2. Crie um script que leia o arquivo capturado e: </br>
  Conte quantos pacotes foram capturados no Total;</br>
  Conte quantos pacotes possuem protocolo de camada de Rede "IP"</br>
Conte quantos pacotes possuem protocolo de camada de Transporte "TCP"</br>
  Conte quantos pacotes possuem protocolo de camada de Transporte "UDP"</br>
  Separe as sessões "TCP" e "UDP" em dicionários de listas (chave == sessão, valor recebe lista de payloads daquela sessão em formato legível) </br>
  Imprima a quantidade de sessões TCP e UDP, bem como a quantidade de pacotes não-associados ao protocolo de rede IP.
